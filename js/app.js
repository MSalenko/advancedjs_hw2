// Теоретичне питання: Конструкція try...catch використовується для обробки помилок, які можуть виникнути
//  під час виконання коду. Наприклад для перевірки правильності вводу користувачем будь-яких даних, чи
// перевірки наявності або відсутності властивостей об'єкта, як в задачі нижче, і таке інше.

const books = [
  {
    author: 'Люсі Фолі',
    name: 'Список запрошених',
    price: 70,
  },
  {
    author: 'Сюзанна Кларк',
    name: 'Джонатан Стрейндж і м-р Норрелл',
  },
  {
    name: 'Дизайн. Книга для недизайнерів.',
    price: 70,
  },
  {
    author: 'Алан Мур',
    name: 'Неономікон',
    price: 70,
  },
  {
    author: 'Террі Пратчетт',
    name: 'Рухомі картинки',
    price: 40,
  },
  {
    author: 'Анґус Гайленд',
    name: 'Коти в мистецтві',
  },
];

const keys = ['author', 'name', 'price'];
const div = document.getElementById('root');
const ul = document.createElement('ul');
div.appendChild(ul);

function appendBook(book) {
  const li = document.createElement('li');
  ul.append(li);
  for (let key in book) {
    const p = document.createElement('p');
    p.innerHTML = `${key}: ${book[key]}`;
    li.append(p);
  }
}

function createBooksList(books) {
  books.forEach(book => {
    const bookKeys = Object.keys(book);
    if (JSON.stringify(bookKeys) === JSON.stringify(keys)) {
      appendBook(book);
    } 
    else {
      try {
        keys.forEach(key => {
          if (!bookKeys.includes(key)) {
            throw new Error(`There is no ${key} in this book!`);
          }
        });
      } catch (error) {
        console.warn(error.message);
      }
    }
  });
}

createBooksList(books);